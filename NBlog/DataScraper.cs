﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using NBlog;
using System.Net;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Text.RegularExpressions;

class DataScraper : DriverController
{
    WebClient wc = new WebClient();
    HtmlDocument doc = new HtmlDocument();
    Jurassic.ScriptEngine engine = new Jurassic.ScriptEngine();

    string blogSearchUrl = "https://section.blog.naver.com/Search/Blog.nhn?pageNo={1}&orderBy=sim&keyword={0}";
    string detailUrl = "https://section.blog.naver.com/ajax/SearchList.nhn?countPerPage=10&currentPage=1&keyword={0}&orderBy=sim&type=blog";

    string stepUrl = "https://section.blog.naver.com/ajax/BlogUserInfo.nhn";

    public readonly int maxPage = 400;

    string blogUrl = "https://blog.naver.com/PostList.nhn?from=postList&blogId={0}&currentPage={1}";

    public DataScraper()
    {
        wc.Headers.Clear();

        wc.Headers.Add("Cache-Control", "no-cache");
        wc.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
        wc.Headers.Add("Content-Type", "application/javascript,text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        wc.Headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        wc.Encoding = Encoding.UTF8;
    }

    string GetID(string blogUrl_)
    {
        blogUrl_ = blogUrl_.Replace("http://", "");
        if (string.IsNullOrEmpty(blogUrl_))
            return "";
        if (blogUrl_.Contains("/") == false)
            return "";

        string id = blogUrl_.Substring(blogUrl_.LastIndexOf('/') + 1);
        if (id.Contains("?blogId"))
        {
            string[] arrID = id.Split('?');
            id = arrID[0].Substring(arrID[0].LastIndexOf('=') + 1);
        }

        return id;
    }

    public void DoParse(string keyWord_)
    {
        CreatDriver();

        for (int i=Global.numericUpDown_PageStart; i<Global.numericUpDown_PageEnd; ++i)
        {
            if (Global.stop)
                break;

            string url = string.Format(blogSearchUrl, keyWord_, i);

            Global.uiController.PrintLog(keyWord_ + " / " + i + "페이지");

            try
            {
                driver.Navigate().GoToUrl(url);
                if (driver.PageSource.Contains("대한 검색결과가 없습니다")
                    || driver.PageSource.Contains("list_search_blog") == false)
                {
                    Global.uiController.PrintLog(keyWord_ + " 에 대한 검색 결과가 없습니다.");
                    break;
                }

                Thread.Sleep(3000);

                IList<IWebElement> listBlogNode = FindMyElements(By.ClassName("list_search_blog"));
                foreach (var item in listBlogNode)
                {
                    if (Global.stop)
                        break;

                    IWebElement titleNode = item.FindElement(By.ClassName("name_blog"));
                    string urlBlog = titleNode.GetAttribute("ng-href");
                    Global.PrintLog(titleNode.Text + " / " + urlBlog);

                    string id = GetID(urlBlog);

                    ParseBlog(id);
                }
            }
            catch
            {
                continue;
            }                    
            
                     
        }

        QuiteDriver();
    }

    public void ParseBlog(string id_)
    {
        DriverController temp = new DriverController();
        temp.CreatDriver();

        for (int i=1; ;++i)
        {
            if (Global.stop)
                break;

            string url = string.Format(blogUrl, id_ , i);        
            try
            {
                temp.driver.Navigate().GoToUrl(url);
                Thread.Sleep(1000);

                if (temp.driver.PageSource.Contains("아직 작성된 글이 없습니다."))
                    break;

                IWebElement postListBody = temp.driver.FindElement(By.Id("postListBody"));
                IList<IWebElement> listPost = postListBody.FindElements(By.XPath("./div"));
                foreach (var item in listPost)
                {
                    if (Global.stop)
                        break;

                    string className = item.GetAttribute("class");
                    if (className.Contains("post _post_wrap") == false)
                        continue;

                    if (ParsePost(temp.driver, item) == false)
                    {
                        temp.QuiteDriver();
                        return;
                    }
                }

                Global.uiController.UpdateDataLog();
            }
            catch
            {
                Global.uiController.PrintLog("IP 블럭으로 인한 변경");
#if TEST
#else
                Global.IP_Change();
#endif

                break;
            }
                                 
            
        }

        temp.QuiteDriver();
    }

    public bool ParsePost(ChromeDriver driver_, IWebElement post_, bool isPost_ = true)
    {
        //이 케이스는 성공으로 패스 한다.
        string stringHtml = post_.GetAttribute("innerHTML");
        if (stringHtml.Contains("wrap_postcomment") == false)
            return true;


        try
        {
            IWebElement postElm = post_.FindElement(By.ClassName("url"));
            string postNum = postElm.GetAttribute("id").Split('_').ToList().Last();
            if (Global.listPostNum.Contains(postNum))
                return false;
            else
                Global.listPostNum.Add(postNum);

            IWebElement groupBtn = post_.FindElement(By.ClassName("wrap_postcomment"));
            IList<IWebElement> listBtn = groupBtn.FindElements(By.XPath(".//div"));
            foreach (var item in listBtn)
            {
                string className = item.GetAttribute("class");

                //try
                {
                    if (className.Contains("sympathy"))
                    {
                        ParseSympathy(driver_, item, postNum);
                    }
                    else if (className.Contains("area_comment"))
                    {
                        ParseComment(driver_, item, postNum);
                    }
                }
                //catch
                {

                }
            }
        }
        catch
        {

        }

        

        return true;
    }

    void ParseSympathy(ChromeDriver driver_, IWebElement bnt_ , string postNum_)
    {
        string symCnt = bnt_.Text;
        symCnt = Regex.Replace(symCnt, @"\D", "");
        if (string.IsNullOrEmpty(symCnt) || symCnt == "0")
            return;

        bnt_.Click();
        Thread.Sleep(1000);

        driver_.SwitchTo().Frame("sympathyFrm" + postNum_);

        IList<IWebElement> listSym = driver_.FindElements(By.XPath("//*[@id='post-area']/ul/li"));
        foreach (var sym in listSym)
        {
            string html = sym.GetAttribute("innerHTML");
            if (html.Contains("탈퇴한"))
                continue;

            IWebElement address = sym.FindElement(By.ClassName("pcol2"));
            string urlBlog = address.GetAttribute("href");
            string id = GetID(urlBlog);

            if (id.Contains("DomainDispatcher"))
                continue;

            if (string.IsNullOrEmpty(id))
                continue;

            if (Global.listIdData.Contains(id) == false)
            {
                Global.listIdData.Add(id);
                Global.uiController.PrintLogID(id);
            }
        }
        driver_.SwitchTo().DefaultContent();
    }

    void ParseComment(ChromeDriver driver_, IWebElement bnt_, string postNum_)
    {
        string cnt = bnt_.Text;
        cnt = Regex.Replace(cnt, @"\D", "");
        if (string.IsNullOrEmpty(cnt) || cnt == "0")
            return;

        bnt_.Click();
        Thread.Sleep(1000);

        IWebElement commentRoot = null;
          
        try
        {
            commentRoot = driver_.FindElement(By.ClassName("u_cbox_content_wrap"));
        }
        catch
        {
            return;
        }


        IList<IWebElement> listComment = commentRoot.FindElements(By.XPath(".//ul/li"));
        foreach(var item in listComment)
        {
            IWebElement address = null;
            try
            {
                address = item.FindElement(By.ClassName("u_cbox_name"));
            }
            catch
            {
                continue;
            }

            string blogUrl = address.GetAttribute("href");
            if (string.IsNullOrEmpty(blogUrl))
                continue;

            string id = GetID(blogUrl);
            if (string.IsNullOrEmpty(id))
                continue;

            if (Global.listIdData.Contains(id) == false)
            {
                Global.listIdData.Add(id);
                Global.uiController.PrintLogID(id);
            }
        }
    }
}

