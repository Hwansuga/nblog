﻿namespace NBlog
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.listBox_Log = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel_Setting = new System.Windows.Forms.TableLayoutPanel();
            this.radioButton_Tethering2 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label_CntCollectData = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown_PageStart = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_PageEnd = new System.Windows.Forms.NumericUpDown();
            this.listBox_TargetKeyWord = new System.Windows.Forms.ListBox();
            this.checkBox_Repeat = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDown_MinDelay = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label_KeyWord = new System.Windows.Forms.Label();
            this.radioButton_Tethering1 = new System.Windows.Forms.RadioButton();
            this.button_Start = new System.Windows.Forms.Button();
            this.button_Stop = new System.Windows.Forms.Button();
            this.button_Save = new System.Windows.Forms.Button();
            this.listBox_LogID = new System.Windows.Forms.ListBox();
            this.checkBox_MailType = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel_Setting.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PageStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PageEnd)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MinDelay)).BeginInit();
            this.SuspendLayout();
            // 
            // listBox_Log
            // 
            this.listBox_Log.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox_Log.FormattingEnabled = true;
            this.listBox_Log.ItemHeight = 12;
            this.listBox_Log.Location = new System.Drawing.Point(401, 12);
            this.listBox_Log.Name = "listBox_Log";
            this.listBox_Log.Size = new System.Drawing.Size(385, 316);
            this.listBox_Log.TabIndex = 0;
            // 
            // tableLayoutPanel_Setting
            // 
            this.tableLayoutPanel_Setting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tableLayoutPanel_Setting.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tableLayoutPanel_Setting.ColumnCount = 2;
            this.tableLayoutPanel_Setting.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.69231F));
            this.tableLayoutPanel_Setting.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.30769F));
            this.tableLayoutPanel_Setting.Controls.Add(this.radioButton_Tethering2, 1, 3);
            this.tableLayoutPanel_Setting.Controls.Add(this.label4, 0, 5);
            this.tableLayoutPanel_Setting.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel_Setting.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel_Setting.Controls.Add(this.label_CntCollectData, 1, 5);
            this.tableLayoutPanel_Setting.Controls.Add(this.tableLayoutPanel2, 1, 1);
            this.tableLayoutPanel_Setting.Controls.Add(this.listBox_TargetKeyWord, 1, 0);
            this.tableLayoutPanel_Setting.Controls.Add(this.checkBox_Repeat, 0, 2);
            this.tableLayoutPanel_Setting.Controls.Add(this.tableLayoutPanel3, 1, 2);
            this.tableLayoutPanel_Setting.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel_Setting.Controls.Add(this.label_KeyWord, 1, 4);
            this.tableLayoutPanel_Setting.Controls.Add(this.radioButton_Tethering1, 0, 3);
            this.tableLayoutPanel_Setting.Location = new System.Drawing.Point(13, 13);
            this.tableLayoutPanel_Setting.Name = "tableLayoutPanel_Setting";
            this.tableLayoutPanel_Setting.RowCount = 6;
            this.tableLayoutPanel_Setting.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.04895F));
            this.tableLayoutPanel_Setting.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.79021F));
            this.tableLayoutPanel_Setting.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.58741F));
            this.tableLayoutPanel_Setting.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel_Setting.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.741259F));
            this.tableLayoutPanel_Setting.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.79021F));
            this.tableLayoutPanel_Setting.Size = new System.Drawing.Size(195, 286);
            this.tableLayoutPanel_Setting.TabIndex = 1;
            // 
            // radioButton_Tethering2
            // 
            this.radioButton_Tethering2.AutoSize = true;
            this.radioButton_Tethering2.Checked = true;
            this.radioButton_Tethering2.Location = new System.Drawing.Point(96, 213);
            this.radioButton_Tethering2.Name = "radioButton_Tethering2";
            this.radioButton_Tethering2.Size = new System.Drawing.Size(65, 16);
            this.radioButton_Tethering2.TabIndex = 15;
            this.radioButton_Tethering2.TabStop = true;
            this.radioButton_Tethering2.Text = "테더링2";
            this.radioButton_Tethering2.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(1, 258);
            this.label4.Margin = new System.Windows.Forms.Padding(1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 27);
            this.label4.TabIndex = 6;
            this.label4.Text = "수집된 데이터";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(1, 147);
            this.label2.Margin = new System.Windows.Forms.Padding(1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "블로그 페이지";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(1, 1);
            this.label1.Margin = new System.Windows.Forms.Padding(1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 144);
            this.label1.TabIndex = 0;
            this.label1.Text = "키워드";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_CntCollectData
            // 
            this.label_CntCollectData.AutoSize = true;
            this.label_CntCollectData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_CntCollectData.Location = new System.Drawing.Point(94, 258);
            this.label_CntCollectData.Margin = new System.Windows.Forms.Padding(1);
            this.label_CntCollectData.Name = "label_CntCollectData";
            this.label_CntCollectData.Size = new System.Drawing.Size(100, 27);
            this.label_CntCollectData.TabIndex = 7;
            this.label_CntCollectData.Text = "0";
            this.label_CntCollectData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.44444F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45454F));
            this.tableLayoutPanel2.Controls.Add(this.label5, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown_PageStart, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.numericUpDown_PageEnd, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(94, 147);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(100, 26);
            this.tableLayoutPanel2.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(45, 1);
            this.label5.Margin = new System.Windows.Forms.Padding(1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(7, 24);
            this.label5.TabIndex = 5;
            this.label5.Text = "~";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_PageStart
            // 
            this.numericUpDown_PageStart.Location = new System.Drawing.Point(3, 3);
            this.numericUpDown_PageStart.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.numericUpDown_PageStart.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_PageStart.Name = "numericUpDown_PageStart";
            this.numericUpDown_PageStart.Size = new System.Drawing.Size(38, 21);
            this.numericUpDown_PageStart.TabIndex = 0;
            this.numericUpDown_PageStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_PageStart.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDown_PageEnd
            // 
            this.numericUpDown_PageEnd.Location = new System.Drawing.Point(56, 3);
            this.numericUpDown_PageEnd.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.numericUpDown_PageEnd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_PageEnd.Name = "numericUpDown_PageEnd";
            this.numericUpDown_PageEnd.Size = new System.Drawing.Size(39, 21);
            this.numericUpDown_PageEnd.TabIndex = 1;
            this.numericUpDown_PageEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_PageEnd.Value = new decimal(new int[] {
            400,
            0,
            0,
            0});
            // 
            // listBox_TargetKeyWord
            // 
            this.listBox_TargetKeyWord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox_TargetKeyWord.FormattingEnabled = true;
            this.listBox_TargetKeyWord.ItemHeight = 12;
            this.listBox_TargetKeyWord.Location = new System.Drawing.Point(96, 3);
            this.listBox_TargetKeyWord.Name = "listBox_TargetKeyWord";
            this.listBox_TargetKeyWord.Size = new System.Drawing.Size(96, 140);
            this.listBox_TargetKeyWord.TabIndex = 9;
            // 
            // checkBox_Repeat
            // 
            this.checkBox_Repeat.AutoSize = true;
            this.checkBox_Repeat.Location = new System.Drawing.Point(3, 177);
            this.checkBox_Repeat.Name = "checkBox_Repeat";
            this.checkBox_Repeat.Size = new System.Drawing.Size(48, 16);
            this.checkBox_Repeat.TabIndex = 10;
            this.checkBox_Repeat.Text = "반복";
            this.checkBox_Repeat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox_Repeat.UseVisualStyleBackColor = true;
            this.checkBox_Repeat.Visible = false;
            this.checkBox_Repeat.CheckedChanged += new System.EventHandler(this.checkBox_Repeat_CheckedChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.38384F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.61616F));
            this.tableLayoutPanel3.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_MinDelay, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(94, 175);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(100, 34);
            this.tableLayoutPanel3.TabIndex = 11;
            this.tableLayoutPanel3.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(41, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 34);
            this.label3.TabIndex = 0;
            this.label3.Text = "분 딜레이";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Visible = false;
            // 
            // numericUpDown_MinDelay
            // 
            this.numericUpDown_MinDelay.Enabled = false;
            this.numericUpDown_MinDelay.Location = new System.Drawing.Point(3, 3);
            this.numericUpDown_MinDelay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_MinDelay.Name = "numericUpDown_MinDelay";
            this.numericUpDown_MinDelay.Size = new System.Drawing.Size(32, 21);
            this.numericUpDown_MinDelay.TabIndex = 1;
            this.numericUpDown_MinDelay.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(1, 233);
            this.label6.Margin = new System.Windows.Forms.Padding(1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 23);
            this.label6.TabIndex = 12;
            this.label6.Text = "진행 키워드";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_KeyWord
            // 
            this.label_KeyWord.AutoSize = true;
            this.label_KeyWord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_KeyWord.Location = new System.Drawing.Point(94, 233);
            this.label_KeyWord.Margin = new System.Windows.Forms.Padding(1);
            this.label_KeyWord.Name = "label_KeyWord";
            this.label_KeyWord.Size = new System.Drawing.Size(100, 23);
            this.label_KeyWord.TabIndex = 13;
            this.label_KeyWord.Text = "None";
            this.label_KeyWord.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radioButton_Tethering1
            // 
            this.radioButton_Tethering1.AutoSize = true;
            this.radioButton_Tethering1.Location = new System.Drawing.Point(3, 213);
            this.radioButton_Tethering1.Name = "radioButton_Tethering1";
            this.radioButton_Tethering1.Size = new System.Drawing.Size(65, 16);
            this.radioButton_Tethering1.TabIndex = 14;
            this.radioButton_Tethering1.Text = "테더링1";
            this.radioButton_Tethering1.UseVisualStyleBackColor = true;
            this.radioButton_Tethering1.CheckedChanged += new System.EventHandler(this.radioButton_Tethering1_CheckedChanged);
            // 
            // button_Start
            // 
            this.button_Start.Location = new System.Drawing.Point(13, 305);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(89, 23);
            this.button_Start.TabIndex = 2;
            this.button_Start.Text = "시작";
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // button_Stop
            // 
            this.button_Stop.Enabled = false;
            this.button_Stop.Location = new System.Drawing.Point(108, 305);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(100, 23);
            this.button_Stop.TabIndex = 3;
            this.button_Stop.Text = "정지";
            this.button_Stop.UseVisualStyleBackColor = true;
            this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
            // 
            // button_Save
            // 
            this.button_Save.Location = new System.Drawing.Point(214, 305);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(177, 23);
            this.button_Save.TabIndex = 4;
            this.button_Save.Text = "저장";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // listBox_LogID
            // 
            this.listBox_LogID.FormattingEnabled = true;
            this.listBox_LogID.ItemHeight = 12;
            this.listBox_LogID.Location = new System.Drawing.Point(214, 12);
            this.listBox_LogID.Name = "listBox_LogID";
            this.listBox_LogID.Size = new System.Drawing.Size(177, 256);
            this.listBox_LogID.TabIndex = 5;
            // 
            // checkBox_MailType
            // 
            this.checkBox_MailType.AutoSize = true;
            this.checkBox_MailType.Location = new System.Drawing.Point(215, 275);
            this.checkBox_MailType.Name = "checkBox_MailType";
            this.checkBox_MailType.Size = new System.Drawing.Size(96, 16);
            this.checkBox_MailType.TabIndex = 6;
            this.checkBox_MailType.Text = "메일주소형태";
            this.checkBox_MailType.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 348);
            this.Controls.Add(this.checkBox_MailType);
            this.Controls.Add(this.listBox_LogID);
            this.Controls.Add(this.button_Save);
            this.Controls.Add(this.button_Stop);
            this.Controls.Add(this.button_Start);
            this.Controls.Add(this.tableLayoutPanel_Setting);
            this.Controls.Add(this.listBox_Log);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "NBlog";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel_Setting.ResumeLayout(false);
            this.tableLayoutPanel_Setting.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PageStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PageEnd)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MinDelay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_Log;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Setting;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_CntCollectData;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDown_PageStart;
        private System.Windows.Forms.NumericUpDown numericUpDown_PageEnd;
        private System.Windows.Forms.ListBox listBox_LogID;
        private System.Windows.Forms.ListBox listBox_TargetKeyWord;
        private System.Windows.Forms.CheckBox checkBox_Repeat;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDown_MinDelay;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_KeyWord;
        private System.Windows.Forms.RadioButton radioButton_Tethering2;
        private System.Windows.Forms.RadioButton radioButton_Tethering1;
        private System.Windows.Forms.CheckBox checkBox_MailType;
    }
}

