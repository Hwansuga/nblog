﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NBlog;
using System.IO;
using System.Threading;

class Global : Singleton<Global>
{
    public static bool stop = false;

    public static Form1 uiController = null;

    public static string textBox_Keyword = "";

    public static int numericUpDown_PageStart = 1;
    public static int numericUpDown_PageEnd = 400;
    public static bool checkBox_Repeat = false;
    public static int numericUpDown_MinDelay = 1;
    public static bool radioButton_Tethering1 = false;

    public static List<string> listIdData = new List<string>();
    public static List<string> listPostNum = new List<string>();


    public static List<string> listTargetKeyWord = new List<string>();

    public static bool checkBox_MailType = false;

    public static void LoadTargetKeyWord()
    {
        Util.LoadTxtFile("TargetKeyWord.txt", ref listTargetKeyWord);
    }


    public static void PrintLog(string msg_)
    {
        if (uiController == null)
            return;

        uiController.PrintLog(msg_);
    }

    public static void PrintLogID(string msg_)
    {
        if (uiController == null)
            return;

        uiController.PrintLogID(msg_);
    }

    public static void IP_Change()
    {
        uiController.PrintLog("아이피 변경중");
        if (radioButton_Tethering1)
            IPVer1();
        else
            IPVer2();
    }

    public static void IPVer1()
    {
        while (true)
        {
            if (stop)
                break;

            try
            {
                string OldIP = Util.Get_IP();
                uiController.PrintLog("비행기 모드 ON");
                Util.Adb_Send("shell settings put global airplane_mode_on 1");
                Util.Adb_Send("shell am broadcast -a android.intent.action.AIRPLANE_MODE");
                Thread.Sleep(5000);
                uiController.PrintLog("비행기 모드 OFF");
                Util.Adb_Send("shell settings put global airplane_mode_on 0");
                Util.Adb_Send("shell am broadcast -a android.intent.action.AIRPLANE_MODE");
                Thread.Sleep(5000);
                for (int i = 0; i < 10; i++)
                {
                    string NewIP = Util.Get_IP();
                    if (NewIP != OldIP)
                    {
                        uiController.PrintLog("현재 아이피 : " + NewIP + " / 이전 : " + OldIP.ToString());
                        return;
                    }

                    Thread.Sleep(1000);
                }

            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                uiController.PrintLog("비행기 모드 OFF");
                Util.Adb_Send("shell settings put global airplane_mode_on 0");
                Util.Adb_Send("shell am broadcast -a android.intent.action.AIRPLANE_MODE");
                uiController.PrintLog("아이피 변경 오류 10초후 재시도 합니다.");
                Thread.Sleep(10000);
            }
        }
    }



    public static void IPVer2()
    {
        while (true)
        {
            if (stop)
                break;

            try
            {
                string OldIP = Util.Get_IP();
                uiController.PrintLog("비행기 모드 ON");
                Util.Adb_Send("shell svc data disable");
                Thread.Sleep(5000);
                uiController.PrintLog("비행기 모드 OFF");
                Util.Adb_Send("shell svc data enable");
                Thread.Sleep(5000);
                for (int i = 0; i < 10; i++)
                {
                    string NewIP = Util.Get_IP();
                    if (NewIP != OldIP)
                    {
                        uiController.PrintLog("현재 아이피 : " + NewIP + " / 이전 : " + OldIP.ToString());
                        return;
                    }

                    Thread.Sleep(2000);
                }

            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                uiController.PrintLog("비행기 모드 OFF");
                Util.Adb_Send("shell settings put global airplane_mode_on 0");
                Util.Adb_Send("shell am broadcast -a android.intent.action.AIRPLANE_MODE");
                uiController.PrintLog("아이피 변경 오류 10초후 재시도 합니다.");
                Thread.Sleep(10000);
            }
        }
    }
}
