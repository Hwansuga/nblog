﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NBlog
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void PrintLog(string msg_)
        {
            try
            {
                listBox_Log.Invoke(new MethodInvoker(delegate ()
                {
                    Util.AutoLineStringAdd(listBox_Log, msg_);
                }));
            }
            catch
            {
                listBox_Log.Invoke(new MethodInvoker(delegate ()
                {
                    listBox_Log.Items.Clear();
                    Util.AutoLineStringAdd(listBox_Log, "로그 정리");
                    Util.AutoLineStringAdd(listBox_Log, msg_);
                }));
            }
            
        }

        public void PrintLogID(string msg_)
        {
            listBox_LogID.Invoke(new MethodInvoker(delegate ()
            {
                Util.AutoLineStringAdd(listBox_LogID, msg_);
            }));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Util.KillProcess("chrome");
            Global.uiController = this;
            EventUtil.ConnectCustomEvent(this);

            Global.LoadTargetKeyWord();
            foreach (var item in Global.listTargetKeyWord)
                listBox_TargetKeyWord.Items.Add(item);

        }

        public void UpdateKeyword(string keyWord_)
        {
            label_KeyWord.Invoke(new MethodInvoker(delegate() {
                label_KeyWord.Text = keyWord_;
            }));
        }

        public void UpdateDataLog()
        {
            this.Invoke(new MethodInvoker(delegate () {
                label_CntCollectData.Text = Global.listIdData.Count.ToString();
            }));
        }

        private void button_Start_Click(object sender, EventArgs e)
        {
            Global.stop = false;

            Thread hCollectWorker = new Thread(WorkThread);
            hCollectWorker.Start();

            PrintLog("시작");

            this.Invoke(new MethodInvoker(delegate ()
            {
                tableLayoutPanel_Setting.Enabled = false;               
                button_Stop.Enabled = true;
                button_Save.Enabled = false;

                button_Start.Enabled = false;
            }));


        }

        void WorkThread()
        {
            DataScraper dataScraper = new DataScraper();

            //while(Global.stop == false)
            {              
                foreach(var item in Global.listTargetKeyWord)
                {
                    if (Global.stop)
                        break;

                    UpdateKeyword(item);
                    try
                    {
                        dataScraper.DoParse(item);
                    }
                    catch
                    {
                        continue;
                    }
                    
                }
            }

            dataScraper.QuiteDriver();

            PrintLog("정지 완료");

            this.Invoke(new MethodInvoker(delegate () {
                tableLayoutPanel_Setting.Enabled = true;
                button_Start.Enabled = true;               
                button_Save.Enabled = true;

                button_Stop.Enabled = false;
            }));
        }

        private void checkBox_Repeat_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDown_MinDelay.Enabled = checkBox_Repeat.Checked;
        }

        private void button_Stop_Click(object sender, EventArgs e)
        {
            Global.stop = true;

            PrintLog("정지중...");

            this.Invoke(new MethodInvoker(delegate ()
            {
                button_Stop.Enabled = false;
            }));
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            string curTime = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
            string fileName = "Data_" + curTime + ".txt";
            if (Global.checkBox_MailType)
                Util.SaveTxtFile(fileName, Global.listIdData.ConvertAll(x=> x + "@naver.com"),  "Export");
            else
                Util.SaveTxtFile(fileName, Global.listIdData, "Export");
            PrintLog(fileName + " 저장 완료");
        }

        private void radioButton_Tethering1_CheckedChanged(object sender, EventArgs e)
        {
            radioButton_Tethering2.Checked = !radioButton_Tethering1.Checked;
        }
    }
}
